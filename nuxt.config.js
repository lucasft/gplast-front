module.exports = {
  /*
  ** Build configuration
  */
  build: {
    vendor: ['axios', 'vuetify']
  },
  /*
  ** Headers
  ** Common headers are already provided by @nuxtjs/pwa preset
  */
  head: {},
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Customize app manifest
  */
  manifest: {
    theme_color: '#3B8070'
  },
  /*
  ** Modules
  */
  modules: [
    '@nuxtjs/vuetify',
    '@nuxtjs/pwa',
    'nuxt-fontawesome',
    '@nuxtjs/axios',
  ],
  axios: {
    baseURL: process.env.BASE_URL || 'http://18.228.192.240:3015/api/',
    browserBaseURL: process.env.BASE_URL_BROWSER || process.env.BASE_URL || 'http://18.228.192.240:3015/api/',
  },
}
